let collection = [];


// Function to print queue elements
function print() {
  console.log("RESULT =", collection);
  return collection;
}

// Function to enqueue a new element
function enqueue(value) {
  collection.push(value);
  console.log("The value has been enqueued. RESULT =", collection);
  return collection;
}

// Function to dequeue the first element
function dequeue() {
  const dequeuedValue = collection.shift();
  console.log("The value has been dequeued. RESULT =", collection);
  return collection;
}

// Function to get the first element
function front() {
  const firstValue = collection[0];
  console.log("The first value has been retrieved. RESULT =", firstValue);
  return firstValue;
}

// Function to get the queue size
function size() {
  const size = collection.length;
  console.log("The size of the queue has been retrieved. RESULT =", size);
  return size;
}

function isEmpty() {
  return this.collection.length === 0; 

}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};